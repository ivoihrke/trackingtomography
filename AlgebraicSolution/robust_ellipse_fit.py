""" 
this script explores robust ellipse fitting according to 
 
 Halir and Flusser, "NUMERICALLY STABLE DIRECT LEAST SQUARES FITTING OF ELLIPSES", WSCG 1998

 which is a robust implementation of 

 Fitzgibbon, A. W., Pilu, M and Fischer, R. B.: "Direct least squares fitting of ellipses". 
 In Proc. of the 13th International Conference on Pattern Recognition, pages 253–257, Vienna, September 1996.


 Actually, there is an open source implementation by Ben Hammel that looks quite decent:
  https://github.com/bdhammel/least-squares-ellipse-fitting 


(c) 2023 Ivo Ihrke
    Chair for Computational Sensing / Communications Engineering 
    University of Siegen
 """
 
import numpy as np
from matplotlib import pyplot as plt

from csetools.fit.Ellipse import Ellipse


def generate_Ellipse_points( min_angle : float , max_angle : float, N : int, endpoint : bool = True ) -> np.array:
    """
    creates (2, N) array of points on a random Ellipse ( x1...xN; y1...yN )
    within an arc min_angle, ..., max_angle
    
    Returns:
        np.array: _description_
    """    

    #major and minor axis
    sx = np.random.randn( 1 )
    sy = np.random.randn( 1 )

    #rotation
    angle = np.random.randn( 1 ) * np.pi + np.pi

    #midpoint
    tx = np.random.randn( 1 )
    ty = np.random.randn( 1 )

    S = np.eye( 3 )
    S[ 0, 0 ] = sx
    S[ 1, 1 ] = sy

    T = np.eye( 3 )
    T[ 0, 2 ] = tx
    T[ 1, 2 ] = ty
    
    R = np.eye( 3 )
    R[ 0, 0 ] =  np.cos( angle )
    R[ 0, 1 ] = -np.sin( angle )
    R[ 1, 0 ] =  np.sin( angle )
    R[ 1, 1 ] =  np.cos( angle )

    #ground truth Ellipsoid
    C = Ellipse.srt_to_ellipsoid( S, R, T )

    #generate points -> this could go to Ellipse, but there it may actually encounter paraboloids or hyperboloids
    angles = np.linspace( -min_angle, max_angle, N, endpoint=endpoint )

    x  = np.cos ( angles ).T
    y  = np.sin ( angles ).T
    w  = np.ones( angles.shape ).T 

    uc = np.vstack( [ x, y, w ] )
    #print( uc.shape )

    transform = np.matmul( T, np.matmul( R, S ) )
    ell  =  np.matmul( transform, uc )

    return ell

def fit_Fitzgibbon_Halir( pts : np.array ) -> Ellipse:

    x = pts[ 0, : ] 
    y = pts[ 1, : ]

    #design matrix part 1 / Halir Eq. 15
    D1 = np.vstack( ( x*x, x*y, y*y ) ).T

    #design matrix part 2 / Halir Eq. 16
    D2 = np.vstack( (x, y, np.ones_like( x ) ) ).T

    #Eq. 17
    S1 = np.matmul( D1.T, D1 )
    S2 = np.matmul( D1.T, D2 )
    S3 = np.matmul( D2.T, D2 )

    #constraint matrix; Eq. 18 
    C1 = np.array( [ [ 0,  0, 2 ], 
                     [ 0, -1, 0 ], 
                     [ 2,  0, 0 ] ] )
    
    T = - np.matmul( np.linalg.inv( S3 ), S2.T )

    #Eq. 26   M a_1 = \lambda a_1 // note negative def. of helper matrix T
    M = np.matmul( np.linalg.inv( C1 ), S1 + np.matmul( S2, T ) )

    [eval, evec] = np.linalg.eig( M )

    #need Eigen vector corresponding to minimum positive Eigen value  
    print( eval )
    print( evec )

    #the evals are sorted in descending order
    index_pos_evals = np.where( eval > 0 )

    smallest_pos_eval_index = index_pos_evals[ -1 ]

    #a1 = evec[ :, smallest_pos_eval_index ]

    # Eigenvector must meet constraint 4ac - b^2 to be valid.
    cond = 4*np.multiply(evec[0, :], evec[2, :]) - np.power(evec[1, :], 2)
    a1   = evec[:, np.nonzero(cond > 0)[0]]
    #print( "a1: {}".format( a1  ) )
    #print( "a1_alt: {}".format( a1_alt ) )
    a2 = np.matmul( T, a1 ) #Eq. 28 
    

    coeffs = np.squeeze( np.vstack( ( a1, a2 ) ).T )
    #print("Ivo-coeffs: {}".format( coeffs ) )

    #create conic matrix / a-dimension independent way to arrange to coefficients; works in principle for higher-dim ellipsoids
    Cd = np.zeros( [ 3, 3 ] )
    Cd[ 0, 0 ] = coeffs[ 0 ]
    Cd[ 0, 1 ] = coeffs[ 1 ]/2
    Cd[ 1, 1 ] = coeffs[ 2 ]
    Cd[ 0, 2 ] = coeffs[ 3 ]/2
    Cd[ 1, 2 ] = coeffs[ 4 ]/2
    Cd[ 2, 2 ] = coeffs[ 5 ]
    C = Cd + Cd.T - np.diag( np.diag ( Cd ) )
   
    return Ellipse( C )

#from https://github.com/bdhammel/least-squares-ellipse-fitting/blob/master/ellipse.py
def fit( X ):
    """Fit the data
    Parameters
    ----------
    X : array, shape (n_points, 2)
        Data values for the x-y data pairs to fit
    Returns
    -------
    self : returns an instance of self.
    """

    # extract x-y pairs
    x, y = X

    # Quadratic part of design matrix [eqn. 15] from (*)
    D1 = np.vstack([x**2, 2*x*y, y**2]).T
    # Linear part of design matrix [eqn. 16] from (*)
    D2 = np.vstack([2*x, 2*y, np.ones_like(x)]).T

    # Forming scatter matrix [eqn. 17] from (*)
    S1 = D1.T @ D1
    S2 = D1.T @ D2
    S3 = D2.T @ D2

    # Constraint matrix [eqn. 18]
    C1 = np.array([[0., 0., 2.], [0., -1., 0.], [2., 0., 0.]])

    # Reduced scatter matrix [eqn. 29]
    M = np.linalg.inv(C1) @ (S1 - S2 @ np.linalg.inv(S3) @ S2.T)

    # M*|a b c >=l|a b c >. Find eigenvalues and eigenvectors from this
    # equation [eqn. 28]
    eigval, eigvec = np.linalg.eig(M)

    # Eigenvector must meet constraint 4ac - b^2 to be valid.
    cond = 4*np.multiply(eigvec[0, :], eigvec[2, :]) - np.power(eigvec[1, :], 2)
    a1 = eigvec[:, np.nonzero(cond > 0)[0]]

    # |d f g> = -S3^(-1) * S2^(T)*|a b c> [eqn. 24]
    a2 = np.linalg.inv(-S3) @ S2.T @ a1

    # Eigenvectors |a b c d f g>
    # list of the coefficients describing an ellipse [a,b,c,d,e,f]
    # corresponding to ax**2 + bxy + cy**2 + dx + ey + f from (*)
    coeffs = np.squeeze( np.vstack([a1, a2]).T )
    
    print("Hammel-coeffs: {}".format( coeffs ))
    #create conic matrix / a-dimension independent way to arrange to coefficients; works in principle for higher-dim ellipsoids
    #Cd = np.zeros( S1.shape )
    #Cd[ np.where( np.triu( np.ones( S1.shape ) ) ) ] = coeffs
    Cd = np.zeros( [ 3, 3 ] )
    Cd[ 0, 0 ]=coeffs[ 0 ]
    Cd[ 0, 1 ]=coeffs[ 1 ]
    Cd[ 1, 1 ]=coeffs[ 2 ]
    Cd[ 0, 2 ]=coeffs[ 3 ]
    Cd[ 1, 2 ]=coeffs[ 4 ]
    Cd[ 2, 2 ]=coeffs[ 5 ]

    C = Cd + Cd.T - np.diag( np.diag ( Cd ) )
   
    
    print( C )
    
    return Ellipse( C )

def main():

    #generate a random ellipsoidal arc; add some noise to the generated points
    pts = generate_Ellipse_points( np.random.randn( 1 ), np.random.randn( 1 ), 32 )
    pts += np.random.randn( pts.shape[ 0 ], pts.shape[ 1 ] ) * 0.005

    ell = fit_Fitzgibbon_Halir( pts )
    #ell = fit( pts[0:2,:] )

    plt.figure()
    plt.plot( pts[ 0, : ], pts[ 1, : ], '.' )
    
    ell.plot( plt.gca(), style='r-' )

if __name__ == "__main__":
    main()
    plt.show()


