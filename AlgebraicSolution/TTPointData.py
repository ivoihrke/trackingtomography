from csetools.algebra.Quaternion import Quaternion
from csetools.fit.Ellipse import Ellipse

from matplotlib import pyplot as plt
import matplotlib.animation as animation
import numpy as np

from typing import List
from copy import deepcopy

class TTRotationSequence:

    rot_list : List[ Quaternion ]

    def __init__( self, N : int ): 
        #N is the number of rotation steps

        mainaxis = np.random.rand( 3 )
        mainaxis /= np.linalg.norm( mainaxis )

        #quaternion interpolation using slerp will use the shortest arc --> max rotation = pi-eps
        steps = np.linspace( 0, np.pi, N, endpoint = False )

        qstart = Quaternion.from_axis_and_angle( mainaxis, steps[  0 ] ) 
        qend   = Quaternion.from_axis_and_angle( mainaxis, steps[ -1 ] ) 
        #print( "qstart: {}".format(steps[0]) )
        #print( "qend: {}".format(steps[-1]) )
        #create a list with N entries and assign interpolated orientations 
        self.rot_list = [ qstart ] * N
        for index, frac in enumerate( np.linspace( 0, 1, N ) ):
            #print( "{}:{}".format( index, frac ) )
            self.rot_list[ index ] = qstart.slerp( qend, frac )
            #print( self.rot_list[ index ])

    def __len__( self ):
        return len ( self.rot_list )

class TTPointData:

    pts : np.array

    rot_sequ : TTRotationSequence

    def __init__( self, N : int, rot_sequ : TTRotationSequence ):
        #N is the number of points

        #random homogeneous point set 
        self.pts = np.random.randn( 4, N )
        self.pts[ 3, : ] = 1

        self.rot_sequ = rot_sequ

    def __len__( self ):
        return self.pts.shape[ 1 ]

    def get_rotated_points( self, idx : int, noise_sigma : float = 0 ):

        if idx >= 0 and idx < len( self.rot_sequ ):
            R = self.rot_sequ.rot_list[ idx ].get_homogeneous_rotation_matrix()

            pts_out = np.matmul( R, self.pts )

            pts_out[ 0:3, : ] += np.random.randn( 3, pts_out.shape[ 1 ] ) * noise_sigma

            return pts_out
        else:
            print("idx out of range")

    def get_tracks( self, noise_sigma : float ) -> np.array:

        N = len( self.rot_sequ ) #number of time frames
        M = len( self ) #number of points

        tracks = np.zeros( [ N, 3, M ] )
        for idx in range( 0, N ):
            pts = self.get_rotated_points( idx, noise_sigma=noise_sigma )
            tracks[ idx, 0:2, : ] = pts[ 0:2, : ]
            tracks[ idx,   2, : ] = 1 

        return TTTracks( tracks )

class TTTracks:
    """
    class to abstract tracks
    - makes handling of real tracks with missing data and other imperfections easier
    """    
    tracks : np.array #this may change -- don't use directly 

    def __init__( self, tracks : np.array ):
        """
        constructor uses 3D arrays [timestep, coordinate, track_id]
        - mark invalid entries by a special entry

        Args:
            tracks (np.array): see above
        """        
        self.tracks = tracks

    def num_tracks( self ):
        return self.tracks.shape[ 2 ]
    
    def num_timesteps( self ):
        return self.tracks.shape[ 0 ]

    def get_point( self, timestep : int, track_id : int ):
        return np.squeeze( self.tracks[ timestep, :, track_id ] )

    def __getitem__( self, track_id : int ):
        return np.swapaxes( self.tracks[ :, :, track_id ], 0, 1 )

class TTEstimator:

    tracks : TTTracks #tracked points 

    #estimate of Ellipse parameters
    solution : np.array

    def __init__( self, tracks : TTTracks ):
        self.tracks = tracks
        self.solution = self.estimate_conic_params()

    def num_ellipses( self ):
        return int( ( len( self.solution ) - 3 ) / 3 )

    def create_joint_estimation_matrix_conic( self, tracks_to_use : TTTracks ):

        #currently, we assume that full data is available; the preprocessing to
        #calling this function should take care of this. This is also, why we do
        #not use self.tracks directly. tracks_to_use is intended to be a subset
        #of self.tracks, suitably selected. 

        L = tracks_to_use.num_tracks()
        N = tracks_to_use.num_timesteps()

        #print("num_tracks: {}, timesteps: {}".format( L, N ) )
        
        #there are N equations for each of the L tracks (each track contains N
        #points, each of them provides one constraint). Therefore, the number of
        #rows is N * L, There are 3 common variables A,B,C, and a set of 3
        #variables D_i, E_i, F_i for each track, i.e 3 * L + 3 variables and
        #therefore columns in the requested matrix.

        A = np.zeros( [ N * L, 3 * L + 3 ] )

        row = 0
        for l in range( L ): #num_tracks
            for n in range( N ): #num_timesteps
                #print( "l: {}, n: {}".format( l, n ) )
                pt = tracks_to_use.get_point( n, l )
                x = pt[ 0 ]
                y = pt[ 1 ]

                A[ row, 0:3 ] = np.array( [ x**2, 2*x*y, y**2 ] ) #constraints on common parameters A,B,C
                coloff = 3 + 3 * l 
                #print("A[{},{}:{}]".format( row, coloff, coloff + 3 ) )
                A[ row, coloff:coloff + 3 ] = np.array( [ 2*x, 2*y, 1 ] )
                

                row += 1 
        
        return A

    def create_estimation_matrices_ellipse( self, tracks_to_use : TTTracks ):

        #currently, we assume that full data is available; the preprocessing to
        #calling this function should take care of this. This is also, why we do
        #not use self.tracks directly. tracks_to_use is intended to be a subset
        #of self.tracks, suitably selected. 

        L = tracks_to_use.num_tracks()
        N = tracks_to_use.num_timesteps()

        #print("num_tracks: {}, timesteps: {}".format( L, N ) )
        
        #there are N equations for each of the L tracks (each track contains N
        #points, each of them provides one constraint). Therefore, the number of
        #rows is N * L, There are 3 common variables A,B,C, and a set of 3
        #variables D_i, E_i, F_i for each track, i.e 3 * L + 3 variables and
        #therefore columns in the requested matrix.

        #A = np.zeros( [ N * L, 3 * L + 3 ] )
        #D1 = np.zeros( [ N * L ], 3 )
        #D2 = [] #there will be L matrices D2 with N*L rows each //WIP//
        S1 = np.zeros( [ 3, 3 ] )
        S2 = [] #a list of L S2 matrices
        S3 = [] #a list of L S3 matrices

        row = 0
        for l in range( L ): #num_tracks
            S2.append( np.zeros( [ 3, 3 ] ) )
            S3.append( np.zeros( [ 3, 3 ] ) )
            
            D1 = np.zeros( [ N, 3 ] )
            D2 = np.zeros( [ N, 3 ] )
        
            for n in range( N ): #num_timesteps
                #print( "l: {}, n: {}".format( l, n ) )
                pt = tracks_to_use.get_point( n, l )
                x = pt[ 0 ]
                y = pt[ 1 ]
                D1[ n, : ] = np.array( [ x**2, 2*x*y, y**2 ] )
                D2[ n, : ] = np.array( [ 2*x, 2*y, 1 ] )

                
            S1      += np.matmul( D1.T, D1 )
            S2[ l ]  = np.matmul( D1.T, D2 )
            S3[ l ]  = np.matmul( D2.T, D2 ) 
        
        return ( S1, S2, S3 )


    def estimate_conic_params( self ):

        tracks_to_use = self.tracks

        #hack: use the first 3 timesteps -- this should be done through the TTTrack interface
        tracks_to_use.tracks = self.tracks.tracks[ 0:36, :, : ]

        A = self.create_joint_estimation_matrix_conic( self.tracks )

        #homogeneous system A*x=0
        [U,D,V] = np.linalg.svd( A )
        #solution is right singular vector corresponding to smallest singular value
        solution = V[-1,:]

        #assemble ellipsoid parameters
        idx = 0 #todo: refactor get_ellipse() or include conversion in Ellipse class 
        A = solution[ 0 ]
        B = solution[ 1 ]
        C = solution[ 2 ]
        D = solution[ 3 + idx * 3     ]
        E = solution[ 3 + idx * 3 + 1 ]
        F = solution[ 3 + idx * 3 + 2 ]
        
        #Ivo Ihrke: Notes on Ellipses, 2004
        C = np.array( [ [ A, B, D ], [B, C, E], [D, E, F] ] )
    
        if not Ellipse.is_ellipsoid( C ):
            #this likely happened because we view the rotation almost in-plane 
            # - in this case, it looks like B^2 - AC > 0 because the major and minor axes have been exchanged

            #this does not work yet !
            print("NOT AN ELLIPSOID - FLIPPING SOLUTION")
            S,R,T = Ellipse.decompose_ellipsoid( C )
            sminor = S[0,0]
            smajor = S[1,1]
            rminor = R[:,0]
            rmajor = R[:,1]
            S[0,0] = smajor
            S[1,1] = sminor
            R[:,0] = rmajor
            R[:,1] = rminor
            C = Ellipse.srt_to_ellipsoid( S, R, T )
            solution[0:3] = Ellipse.parameters( C )[0:3] 

        return solution

    def estimate_ellipse_params( self ):

        tracks_to_use = self.tracks

        #hack: use the first 3 timesteps -- this should be done through the TTTrack interface
        tracks_to_use.tracks = self.tracks.tracks[ 0:5, :, : ]

        S1, S2, S3 = self.create_estimation_matrices_ellipse( tracks_to_use )

        M = S1
        for i in range( len( S2 ) ):
            M -= np.matmul( S2[ i ], np.matmul( np.linalg.inv( S3[ i ] ), S2[ i ].T ) )
        C1 = np.array( [ [ 0, 0, 2], [ 0, -1, 0], [2, 0, 0] ] )
        M = np.matmul( np.linalg.inv( C1 ), M )

        [eval, evec] = np.linalg.eig( M )

        # Eigenvector must meet constraint 4ac - b^2 > 0 (==1) to be valid.
        cond = 4*np.multiply(evec[0, :], evec[2, :]) - np.power(evec[1, :], 2)
        a1   = evec[:, np.nonzero(cond > 0)[ 0 ] ]

        #3 params globally for a1, L x 3 params for a2
        solution = np.zeros( 3 + 3 * len( S2 ) )
        solution[ 0:3 ] = np.squeeze( a1 )

        for i in range( len( S2 ) ):
            solution[ (i+1)*3 : (i+2)*3 ] = np.squeeze( np.matmul( -np.matmul( np.linalg.inv( S3[ i ] ), S2[ i ].T ), a1 ) )

        #assemble ellipsoid parameters
        idx = 0 #todo: refactor get_ellipse() or include conversion in Ellipse class 
        A = solution[ 0 ]
        B = solution[ 1 ]
        C = solution[ 2 ]
        D = solution[ 3 + idx * 3     ]
        E = solution[ 3 + idx * 3 + 1 ]
        F = solution[ 3 + idx * 3 + 2 ]
        
        #Ivo Ihrke: Notes on Ellipses, 2004
        C = np.array( [ [ A, B, D ], [B, C, E], [D, E, F] ] )
    
        return solution

    def get_ellipse( self, idx : int ):

        #assemble ellipsoid parameters
        A = self.solution[ 0 ]
        B = self.solution[ 1 ]
        C = self.solution[ 2 ]
        D = self.solution[ 3 + idx * 3     ]
        E = self.solution[ 3 + idx * 3 + 1 ]
        F = self.solution[ 3 + idx * 3 + 2 ]
        
        #Ivo Ihrke: Notes on Ellipses, 2004
        C = np.array( [ [ A, B, D ], [B, C, E], [D, E, F] ] )
        ell = Ellipse( C )

        return ell 

    def compute_Euclidean_projection( self, ell : Ellipse, pts : np.array, maxiter = 1000 ):

        #do gradient descent until on ellipse
        poe = deepcopy( pts )

        alg_dist = Ellipse.apply_conic( ell.C, poe )
        #print( alg_dist )
        
        iter = 0
        while np.sum( np.fabs( alg_dist ) ) / len( alg_dist ) > 1e-14 and iter < maxiter:
            #sign =  np.matmul( np.ones( [3,1] ), np.sign( [alg_dist] ) )
            sign =  np.matmul( np.ones( [3,1] ), [2*alg_dist] ) #the factor is a step size 
            
            grad = Ellipse.compute_gradient( ell.C, poe )
            poe -= sign * grad
            alg_dist = Ellipse.apply_conic( ell.C, poe )
            iter += 1 
        #print(alg_dist)

        
        if False:
            #this method is not an orthogonal projection onto the ellipse

            S, R, T = Ellipse.decompose_ellipsoid( ell.C )
            Chalf = np.matmul( T, np.matmul( R, S ) )
            #Sinv = np.linalg.inv( S ) #this can be done smarter by inverting the diag elements
            #Rinv = R.transpose()
            #Tinv = np.linalg.inv( T ) #this can be done smarter by changing the [0,2], [1,2] elements to their negative

            #this matrix transforms the ellipse into a unit circle 
            # see Ivo Ihrke, "Notes on Ellipses", 2004
            #Chalf_inv = np.matmul( Sinv, np.matmul( Rinv, Tinv ) ) 
            Chalf_inv = np.linalg.inv( Chalf )

            #points on unit circle
            puc = np.matmul( Chalf_inv, pts )
            norm = np.sqrt( puc[0,:]**2 + puc[1,:]**2  )
            puc[0,:] /= norm
            puc[1,:] /= norm
            #now puc is on the unit circle

            #transform back into ellipse; poe= points on ellipse  
            poe = np.matmul( Chalf, puc )
        return poe


    def compute_Euclidean_distance( self, ell : Ellipse, pts : np.array, maxiter = 1000 ):
        
        poe = self.compute_Euclidean_projection( ell, pts, maxiter=maxiter )
        
        return np.sqrt( np.sum( ( poe - pts )[0:2,:] ** 2, axis=0 ) )
        
    def estimate_projection_of_rotation_axis( self ):

        m = np.zeros( [ 3, self.num_ellipses() ] )
        #print( m.shape )
        for idx in range( self.num_ellipses() ):
            ell = self.get_ellipse( idx )

            m[ :, idx ] = ell.get_center()

        mmed = np.median( m, axis = 1 )

        S,R,T = Ellipse.decompose_ellipsoid( ell.C )

        radir = R[:,0] #the minor axis

        return mmed, radir 
    
    def line_explicit_to_implicit( self, pos, dir ):

        px = pos[ 0 ]
        py = pos[ 1 ]
        dx = dir[ 0 ]
        dy = dir[ 1 ]

        A = np.array( [[px, py, 1],[px+dx, py+dy, 1]])

        #solve Ax=0 
        [U,D,V] = np.linalg.svd( A )

        #last col. of V'
        abc = V.T[:, -1]

        #is ok
        #print( np.matmul( np.array( [px, py, 1] ), abc ) )
        #print( np.matmul( np.array( [px+dx, py+dy, 1] ), abc ) )
        
        return abc

    def estimate_ellipse_centers_on_line( self, ellipse_params, pos, dir ):
        """
        Re-estimates ellipse centres on a given line with given quadratic parameters;
        """        

        v1 = True

        tracks_to_use = self.tracks

        #hack: use the first 3 timesteps -- this should be done through the TTTrack interface
        tracks_to_use.tracks = self.tracks.tracks[ 0:5, :, : ]

        #S1, S2, S3 = self.create_estimation_matrices_ellipse( tracks_to_use )

        A = ellipse_params[ 0 ]
        B = ellipse_params[ 1 ]
        C = ellipse_params[ 2 ]
        
        
        beta = self.line_explicit_to_implicit( pos, dir )

        L = tracks_to_use.num_tracks()
        N = tracks_to_use.num_timesteps()

        solution_out = ellipse_params
        for l in range( L ):
            
            if v1: 
                D2     = np.zeros( [ N, 3 ] )
                gamma2 = np.zeros( [ N ] )
            else:
                #v2
                D2     = np.zeros( [ N, 2 ] )
                gamma2 = np.zeros( [ N ] )
                F      = ellipse_params[ 3 + 3*l + 2 ]

            for n in range( N ):
                pt = tracks_to_use.get_point( n, l )
                x = pt[ 0 ]
                y = pt[ 1 ]

                if v1: 
                    D2[ n, : ] = np.array( [ 2*x, 2*y, 1 ] )
                    gamma2[ n ] = -(A*x**2 + 2*B*x*y + C*y**2)
                else:
                    #v2 
                    D2[ n, : ] = np.array( [ x, y ] )
                    gamma2[ n ] = -(A*x**2 + B*x*y + C*y**2 + F)


            S2 = np.matmul( D2.T, D2 )
            c2 = np.matmul( D2.T, gamma2 )
            print( "c2: {}".format( c2 ) )
            print("------------------")
            
            #v1
            if v1: 
                M = np.zeros( [ 4, 4 ] )
                M[ 0:3, 0:3 ] = S2
                M[ 0:3,  -1 ] = beta/2
                M[   3, 0:3 ] = beta 

                b = np.zeros( [ 4 ] )
                b[0:3] = c2

            #v2
            else:
                M = np.zeros( [ 3, 3 ] )
                M[ 0:2, 0:2 ] = S2
                M[ 0:2,  -1 ] = beta[ 0:2 ] / 2
                M[   2, 0:2 ] = beta[ 0:2 ] 

                b = np.zeros( [ 3 ] )
                b[0:2] = c2
                b[ 2 ] = -beta[ 2 ] * F


            a2_lambda = np.matmul( np.linalg.inv( M ), b  )

            print("a2_l: {}".format( a2_lambda ) )

            if v1: 
                solution_out[ (3 + 3 * l):(3 + 3 * (l+1) ) ] = a2_lambda[ 0:3 ]
            else:
                solution_out[ (3 + 3 * l):(3 + 3 * (l+1) - 1) ] = a2_lambda[ 0:2 ]

        return solution_out


if __name__ == "__main__":
    M = 72
    rot = TTRotationSequence( M )

    pts = TTPointData( 5, rot )

    tracks = pts.get_tracks( noise_sigma=0.01 )

    estimator = TTEstimator( tracks )

    #params = estimator.estimate_ellipse_params()
    estimator.solution = estimator.estimate_ellipse_params()

    plt.figure()
    
    if False:
        ax = plt.axes(projection ='3d')
        def animate_n( j ):
            print( j )
            #print( pts )
            p = pts.get_rotated_points( j )
            ax = plt.axes(projection ='3d')
            #plt.cla()
            #ax = plt.gca()
            ax.plot3D( p[0,:], p[1,:], p[2,:], '.' )

        animation_n = animation.FuncAnimation( plt.gcf(), animate_n, frames=range(0, M), interval=50 )
        animation_n.save("test.mpg")
        #video_n = animation_n.to_html5_video()
        #html_code_n = display.HTML(video_n)
        #display.display(html_code_n)
    
    if False:
        ax = plt.axes(projection ='3d')
        for j in range( 0, len( rot ) ):
            p = pts.get_rotated_points( j )
            ax.plot3D( p[0,:], p[1,:], p[2,:], '.' )
    
    rax, dir = estimator.estimate_projection_of_rotation_axis()
    plt.plot( rax[0], rax[1], 'ro' )
    l = 5
    plt.plot( [rax[0]-l*dir[0], rax[0]+l*dir[0]], [rax[1]-l*dir[1],rax[1]+l*dir[1]], 'g-' )

    
    estimator.solution = estimator.estimate_ellipse_centers_on_line( estimator.solution, rax, dir )

    #print(tracks.tracks)
    ax = plt.gca()
    for idx in range( tracks.num_tracks() ):
        ell = estimator.get_ellipse( idx )
        ell.plot( ax )
        plt.plot( tracks[ idx ][ 0, : ], tracks[ idx ][ 1, : ], '.' )
        poe = estimator.compute_Euclidean_projection( ell, tracks[ idx ] )
        
        #dist = estimator.compute_Euclidean_distance( ell, tracks[ idx ] )
        #plt.plot( poe[0,:], poe[1,:], 'x' )

        print( "distance: {}".format( estimator.compute_Euclidean_distance( ell, tracks[ idx ] ) ) )

    

    plt.tight_layout()
    plt.show()
    
        

