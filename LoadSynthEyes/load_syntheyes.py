import numpy as np
import re

from typing import List

class SynthEyesFrame:

    id           : int 
    cam_center   : np.array
    euler_angles : np.array 

    def __init__( self, id : int, p : np.array, euler : np.array ):

        self.id           = id
        self.cam_center   = p
        self.euler_angles = euler

    def __repr__( self ) -> str:
        out = ("id: {}; C: ( {:0.2e}, {:0.2e}, {:0.2e} ); Euler: ( {:0.2e}, {:0.2e}, {:0.2e} )\n".format( \
            self.id, 
            self.cam_center[ 0 ], 
            self.cam_center[ 1 ],
            self.cam_center[ 2 ],
            self.euler_angles[ 0 ],
            self.euler_angles[ 1 ],
            self.euler_angles[ 2 ]
            ) )
        return out

class SynthEyesSequence:

    frames : List[ SynthEyesFrame ]

    def __init__( self, frames : List[SynthEyesFrame] ):

        self.frames = frames

    def get_camera_centers( self ) -> np.array:
        """
            returns an array of camera centers, 

        Returns:
            np.array: 3 columns with Cx,Cy,Cz and N rows for N frames.
        """        

        centers = np.zeros( [ len( self.frames ), 3 ] )

        for i in range( len( self.frames ) ):
            centers[ i, : ] = self.frames[ i ].cam_center 

        return centers

    def get_euler_angles( self ) -> np.array:
        """
            returns an array of Euler angles

        Returns:
            np.array: 3 columns with Rx,Ry,Rz and N rows for N frames.
        """        

        euler = np.zeros( [ len( self.frames ), 3 ] )

        for i in range( len( self.frames ) ):
            euler[ i, : ] = self.frames[ i ].euler_angles 

        return euler

    def get_ids( self ) -> np.array:

        ids = np.zeros( len( self.frames ) )
        for i in range( len( self.frames ) ):
            ids[ i ] = self.frames[ i ].id

        return ids


def load_syntheyes( filename : str, start_frame : int = 0 ) -> SynthEyesSequence:
    """
    Loads a SynthEyes camera tracking files (txt), format:

    -------------------------------------------------------------------
    header - 1 line:
    Px          Py          Pz          Rx          Ry          Rz          
    
    then, 1 line per camera position; (Px,Py,Pz) are the camera center's 
    position in frame i; (Rx,Ry,Rz) are the Euler angles in degrees;
    TODO: figure out the proper multiplication order 

      0.000    -100.000      10.000      90.000      -0.000       0.000

    -------------------------------------------------------------------

    outputs an array of 

    Args:
        filename (str): a string with the file name

    Returns:
        SynthEyesSequence: a list of individual camera positions and orientations wrapped in a class
    """    
    frames = []

    with open( filename ) as f:

        line = f.readline()

        elem = re.split( "\s+", line )
        #print( elem )
        if  elem[ 0 ] != 'Px' or \
            elem[ 1 ] != 'Py' or \
            elem[ 2 ] != 'Pz' or \
            elem[ 3 ] != 'Rx' or \
            elem[ 4 ] != 'Ry' or \
            elem[ 5 ] != 'Rz':
            
            print( "The file '{}' does not appear to be a SynthEyes camera tracking output file. Returning an empty list ..." )
            return frames
        
        num_frames = 0
        while line:
            line = f.readline()

            elem = re.split( "\s+", line )
            #print( elem )
            if elem[ 0 ] == '':
                elem = elem[1:-1]

            if elem:
                p     = [ float(elem[ 0 ]), float(elem[ 1 ]), float(elem[ 2 ]) ]
                euler = [ float(elem[ 3 ]), float(elem[ 4 ]), float(elem[ 5 ]) ]

                frames.append( SynthEyesFrame( p=p, euler=euler, id=num_frames + start_frame ) )

                num_frames += 1

    return SynthEyesSequence( frames )

#testing the functions
def main():

    filename = "../data/Stack_2_pollen_400_frames_170x1700011.txt"

    frames = load_syntheyes( filename, start_frame = 11 )

    #print( frames )
    centers = frames.get_camera_centers()
    print( centers )

    euler = frames.get_euler_angles()
    print( euler )

    ids = frames.get_ids()
    print( ids )

#if run as a script, start the main() function
if __name__ == "__main__":
    main()
    




